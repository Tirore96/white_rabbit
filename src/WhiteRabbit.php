<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
       $medianLetter = $this->findMedianLetter($this->parseFile($filePath));
       $medianLetterFormatted= $this->formatMedianResult($medianLetter);
       //First median is extracted because testdata assumes one median is returned
       $result = $medianLetterFormatted[0];
       return $result;
    }
    
    private function formatMedianResult($medianResult){
        $retval = [];
        foreach($medianResult as $l => $c){
            array_push($retval,array("letter" => $l, "count" => $c));
        }
        return $retval;
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    //make private
    private function parseFile ($filePath)
    {
        $txtString = file_get_contents($filePath);
        $filteredString= preg_replace("/[^a-zA-Z]+/","",$txtString);

        return $filteredString;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     */

    private function findMedianLetter($parsedFile)
    {
        $occurenceArray = $this->makeHistogram($parsedFile);

        return $this->findMedian($occurenceArray);
    }


    /**
     * Return a histogram over the occurrences of characters in a string
     */
    private function makeHistogram($parsedFile){
        $occurenceArray= [];
        $characterArray = str_split($parsedFile);
        foreach($characterArray as $character){
            $key = strtolower($character);
            if(!empty($occurenceArray[$key])){
                $occurenceArray[$key] += 1;
            }
            else {
                $occurenceArray[$key] = 1;
            }
        }

        return $occurenceArray;
    }

    /**
     * Find characters with median occurrence.
     * Returns assosiative array containing either 1 or 2 medians.
     */
    private function findMedian($occurenceArray){
        $arrayLen = count($occurenceArray);
        $occurenceArraySorted = $occurenceArray;
        asort($occurenceArraySorted);
        $arrayKeys = array_keys($occurenceArraySorted);
        $arrayValues = array_values($occurenceArraySorted);
        $retval = [];

        //Determine indices of medians through length of array
        $indices = (($arrayLen % 2) == 0) ? [$arrayLen/2,($arrayLen/2) + 1] : [floor($arrayLen/2)];
        
        foreach($indices as $index){
            $char  = $arrayKeys[$index];
            $occ = $arrayValues[$index];

            $retval[$char] = $occ;
        }

        return  $retval;
    }
}